import { GatewayService } from "./gateway.service";
import { Injectable } from "@nestjs/common";
import { ModuleRef } from "@nestjs/core";
import { RouteInfo } from "./routerInfo";

@Injectable()
export class RouteService {
  private router = null;

  constructor(private moduleRef: ModuleRef) {}

  private availableRoutes = router =>
    router.stack
      .filter(layer => layer.route)
      .map(
        layer => new RouteInfo(layer.route?.path, layer.route?.stack[0].method),
      );

  getAvailableRoutes(): RouteInfo[] {
    return this.availableRoutes(this.router);
  }

  getMethodsByPath(): Map<String, String[]> {
    const methodByPaths = new Map();
    this.getAvailableRoutes().forEach(route => {
      const path = route.path;
      const method = route.method;
      if (!methodByPaths.has(path)) {
        methodByPaths.set(path, []);
      }
      methodByPaths.get(path).push(method.toUpperCase());
    });
    
    return methodByPaths; 
  }

  routerInit() {
    this.router = (this.moduleRef as any).container.internalProvidersStorage._httpAdapter.httpServer._events.request._router;
  }
}

export const registerGatewayEntries = application => {
  const routeService = application.get(RouteService);
  routeService.routerInit();
  const methodByPaths = routeService.getMethodsByPath();
  const gatewayService = new GatewayService();

  gatewayService.autoRegisterServiceEndpoints(methodByPaths);
};

export const test = () => {
  console.log("test")
};
