export class GatewayService {
  private http = require('http');

  autoRegisterServiceEndpoints(methodByPaths: Map<String, String[]>) {
    const postData = JSON.stringify({
      url: `${process.env.URL}`,
    });

    const options = {
      host: `${process.env.HOST}`,
      port: process.env.GATEWAY_PORT,
      path: `/service-endpoints/${process.env.ENDPOINT}-service`,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `${process.env.APIKEY}`,
      },
    };

    const req = this.http.request(options, res => {
      res.setEncoding('utf8');
      res.resume();
      res.on('end', () => {
        this.autoRegisterEndpoints(0, methodByPaths);
        console.log('No more data in response.');
      });
      req.on('error', e => {
        console.error(`problem with request: ${e.message}`);
      });
    });

    req.write(postData);
    req.end();

    return req;
  }

  autoRegisterEndpoints(i: number, methodsByPaths: Map<String, String[]>) {
    if (i >= methodsByPaths.size) {
      this.autoRegisterPipelines(0, methodsByPaths);
      return;
    }

    const currentKey = [...methodsByPaths.keys()][i];

    const postData = JSON.stringify({
      host: '*',
      path: currentKey,
      methods: methodsByPaths.get(currentKey),
    });

    const options = {
      host: `${process.env.HOST}`,
      port: process.env.GATEWAY_PORT,
      path: `/api-endpoints/${process.env.ENDPOINT}-${currentKey
        .split('/')
        .join('')}`,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `${process.env.APIKEY}`,
      },
    };

    const req = this.http.request(options, res => {
      res.setEncoding('utf8');
      res.resume();
      res.on('end', () => {
        this.autoRegisterEndpoints(i + 1, methodsByPaths);
        console.log('No more data in response.');
      });
      req.on('error', e => {
        console.error(`problem with request: ${e.message}`);
      });
    });

    req.write(postData);
    req.end();
  }

  autoRegisterPipelines(i: number, methodsByPaths: Map<String, String[]>) {
    if (i >= methodsByPaths.size) {
      return;
    }

    const currentKey = [...methodsByPaths.keys()][i];

    const bodyObj = {
      apiEndpoints: [
        `${process.env.ENDPOINT}-${currentKey.split('/').join('')}`,
      ],
      policies: [
        {
          jwt: [
            {
              action: {
                secretOrPublicKey: `${process.env.JWT_SECRET}`,
                checkCredentialExistence: false,
              },
            },
          ],
        },
        {
          proxy: [
            {
              action: {
                changeOrigin: true,
                serviceEndpoint: `${process.env.ENDPOINT}-service`,
              },
            },
          ],
        },
      ],
    };
    if (currentKey === `${process.env.JWT_PATH}`) {
      bodyObj.policies.shift();
    }
    const postData = JSON.stringify(bodyObj);

    const options = {
      host: `${process.env.HOST}`,
      port: process.env.GATEWAY_PORT,
      path: `/pipelines/${process.env.ENDPOINT}-${currentKey
        .split('/')
        .join('')}`,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `${process.env.APIKEY}`,
      },
    };

    const req = this.http.request(options, res => {
      res.setEncoding('utf8');
      res.resume();
      res.on('end', () => {
        this.autoRegisterPipelines(i + 1, methodsByPaths);
        console.log('No more data in response.');
      });
      req.on('error', e => {
        console.error(`problem with request: ${e.message}`);
      });
    });

    req.write(postData);
    req.end();
  }

  autoUnregisterServiceEndpoints(methodsByPaths: Map<String, String[]>) {

    const options = {
      host: `${process.env.HOST}`,
      port: process.env.GATEWAY_PORT,
      path: `/service-endpoints/${process.env.ENDPOINT}-service`,
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `${process.env.APIKEY}`,
      },
    };

    const req = this.http.request(options, res => {
      res.setEncoding('utf8');
      res.resume();
      res.on('end', () => {
        this.autoUnregisterApiEndpoints(0, methodsByPaths);
        console.log('No more data in response.');
      });
      req.on('error', e => {
        console.error(`problem with request: ${e.message}`);
      });
    });

    req.write('');
    req.end();

    return req;
  }

  autoUnregisterApiEndpoints(i: number, methodsByPaths: Map<String, String[]>) {
    if (i >= methodsByPaths.size) {
      this.autoUnregisterPipelines(0, methodsByPaths);
      return;
    }

    const currentKey = [...methodsByPaths.keys()][i];

    const options = {
      host: `${process.env.HOST}`,
      port: process.env.GATEWAY_PORT,
      path: `/api-endpoints/${process.env.ENDPOINT}-${currentKey
        .split('/')
        .join('')}`,
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `${process.env.APIKEY}`,
      },
    };

    const req = this.http.request(options, res => {
      res.setEncoding('utf8');
      res.resume();
      res.on('end', () => {
        this.autoUnregisterApiEndpoints(i + 1, methodsByPaths);
        console.log('No more data in response.');
      });
      req.on('error', e => {
        console.error(`problem with request: ${e.message}`);
      });
    });

    req.write('');
    req.end();
  }

  autoUnregisterPipelines(i: number, methodsByPaths: Map<String, String[]>) {
    if (i >= methodsByPaths.size) {
      return;
    }

    const currentKey = [...methodsByPaths.keys()][i];

    const options = {
      host: `${process.env.HOST}`,
      port: process.env.GATEWAY_PORT,
      path: `/pipelines/${process.env.ENDPOINT}-${currentKey
        .split('/')
        .join('')}`,
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `${process.env.APIKEY}`,
      },
    };

    const req = this.http.request(options, res => {
      res.setEncoding('utf8');
      res.resume();
      res.on('end', () => {
        this.autoUnregisterPipelines(i + 1, methodsByPaths);
        console.log('No more data in response.');
      });
      req.on('error', e => {
        console.error(`problem with request: ${e.message}`);
      });
    });

    req.write('');
    req.end();
  }
}
